Also this obviously gets a bit boring, for me.

```js
const baseUrl = 'http://51.159.25.246';
const authToken = 'e71b4ed2879b84d38a372bfc2b5c1e66-6289378125835c1164dc4dd80eea67d0';
const authKey = '01b666e4d2171f718a813cc22c483d19-d1d90d841ed95d4bf7939ede33380037';
```
**Headers in the request**
```
nestAuth-3Xjf5CB00c-${authToken)
nestKey-0XXh47Udk83J-${authKey}
```
**Example call**
```js
${baseUrl}//b62a7fce6f4504036780058035e80396.php/users?filter[]=email,eq,vqrealnameissamuel@itsover.exposed&__${authToken}&satisfy=all&transform=1&__${authKey}```
**Return value example**
```json
{"users":[{"id_user":"a94c2a17ee898964f95a5eb77fe83896","email":"xxx@xxx.xxx","password":"0e47bbab99bbcab14c260d36df2ea9e7",
"gender":null,"lastname":null,"firstname":null,"birthdate":null,"country":null,"language":"en","currency":"USD",
"notifications":"{\"signals\":true,\"analysis\":true,\"videos\":true,\"advice\":true,\"resources\":true}","nest_premium":"0",
"date_creation":"2019-04-22 04:30:49","date_update":null,"confirmed":0,"optin":"1","status":"active"}]}```
**More**
The whole app works on 5 requests. 
```js
let callUrl = ${baseurl}/b62a7fce6f4504036780058035e80396.php/```
**GetData**
```${callUrl}/users?filter[]=email,eq,...&__${authToken}&satisfy=all&transform=1&__${authKey}```
**postData**
**putData**
**patchData**
**delData**

*Bit lazy to update the rest actually*